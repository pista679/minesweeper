/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*

package minesweeper.core;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

*/
/**
 *
 * @author pista
 *//*

public class FieldTest {

    static final int ROWS = 9;
    static final int COLUMNS = 9;
    static final int MINES = 10;

    public FieldTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    */
/**
     * Test of openTile method, of class Field.
     *//*

    @Test
    public void testOpenTile() {
        System.out.println("Open tile");
        Field field = new Field(ROWS, COLUMNS, MINES);
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                field.openTile(row, column);
                assertNotSame(field.getTile(row, column).getState(), Tile.State.CLOSED);
            }
        }
        
    }

    */
/**
     * Test of markTile method, of class Field.
     *//*

    @Test
    public void testMarkTile() {
        System.out.println("Mark tile");
        Field field = new Field(ROWS, COLUMNS, MINES);
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                field.markTile(row, column);
                assertEquals(field.getTile(row, column).getState(), Tile.State.MARKED);
            }
        }
    }

    */
/**
     * Test of getRowCount method, of class Field.
     *//*

    @Test
    public void testGetRowCount() {
        System.out.println("Get row count");
        Field field = new Field(ROWS, COLUMNS, MINES);
        assertEquals(ROWS, field.getRowCount());
    }

    */
/**
     * Test of getColumnCount method, of class Field.
     *//*

    @Test
    public void testGetColumnCount() {
        System.out.println("Get column count");
        Field field = new Field(ROWS, COLUMNS, MINES);
        assertEquals(COLUMNS, field.getColumnCount());
    }

    */
/**
     * Test of getMineCount method, of class Field.
     *//*

    @Test
    public void testGetMineCount() {
        System.out.println("Get mine count");
        Field field = new Field(ROWS, COLUMNS, MINES);
        assertEquals(MINES, field.getMineCount());
    }

    */
/**
     * Test of getState method, of class Field.
     *//*

    @Test
    public void testGetState() {
        System.out.println("Get state");
        Field field = new Field(ROWS, COLUMNS, MINES);
        assertEquals(field.getState(), GameState.PLAYING);
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                if (!(field.getTile(row, column) instanceof Mine)) {
                    field.openTile(row, column);
                }
            }
        }
        assertEquals(GameState.SOLVED, field.getState());
        assertNotSame(GameState.FAILED, field.getState());

    }

    */
/**
     * Test of getTile method, of class Field.
     *//*

    @Test
    public void testGetTile() {
        System.out.println("Get tile");
        Field field = new Field(ROWS, COLUMNS, MINES);
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                assertNotSame(null, field.getTile(row, column));
            }
        }
    }

    */
/**
     * Test of getRemainingMineCount method, of class Field.
     *//*

    @Test
    public void testGetRemainingMineCount() {
        System.out.println("Get remaining mine count");
        Field field = new Field(ROWS, COLUMNS, MINES);
        int clueCount = 0;
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                assertNotNull(field.getTile(row, column));
                if (field.getTile(row, column) instanceof Clue) {
                    clueCount++;
                }
            }
        }

        assertEquals(ROWS * COLUMNS - MINES, clueCount);
    }

    @Test
    public void isSolved() {
        System.out.println("Is solved");
        Field field = new Field(ROWS, COLUMNS, MINES);

        assertEquals(GameState.PLAYING, field.getState());

        int open = 0;
        for (int row = 0; row < field.getRowCount(); row++) {
            for (int column = 0; column < field.getColumnCount(); column++) {
                if (field.getTile(row, column) instanceof Clue) {
                    field.openTile(row, column);
                    open++;
                }
                if (field.getRowCount() * field.getColumnCount() - open == field.getMineCount()) {
                    assertEquals(GameState.SOLVED, field.getState());
                } else {
                    assertNotSame(GameState.FAILED, field.getState());
                }
            }
        }

        assertEquals(GameState.SOLVED, field.getState());
    }

    @Test
    public void FieldTest() {
        System.out.println("Field");
        Field field = new Field(ROWS, COLUMNS, MINES);

        assertEquals(ROWS, field.getRowCount());
        assertEquals(COLUMNS, field.getColumnCount());
        assertEquals(MINES, field.getMineCount());

    }
}
*/
