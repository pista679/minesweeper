/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*

package minesweeper;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

*/
/**
 *
 * @author pista
 *//*

public class SettingsTest {

    static final int ROWS = 9;
    static final int COLUMNS = 9;
    static final int MINES = 10;
    private static final String SETTING_FILE = System.getProperty("user.home") + System.getProperty("file.separator") + "minesweeper.settings";

    public SettingsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    */
/**
     * Test of getRowCount method, of class Settings.
     *//*

    @Test
    public void testGetRowCount() {
        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        assertEquals(settings.getRowCount(), ROWS);
    }

    */
/**
     * Test of getColumnCount method, of class Settings.
     *//*

    @Test
    public void testGetColumnCount() {
        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        assertEquals(settings.getColumnCount(), COLUMNS);
    }

    */
/**
     * Test of getMineCount method, of class Settings.
     *//*

    @Test
    public void testGetMineCount() {
        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        assertEquals(settings.getMineCount(), MINES);
    }

    */
/**
     * Test of hashCode method, of class Settings.
     *//*

    @Test
    public void testHashCode() {
        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        assertEquals(ROWS * COLUMNS * MINES, settings.hashCode());
    }

    */
/**
     * Test of equals method, of class Settings.
     *//*

    @Test
    public void testEquals() {
        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        assertEquals(settings, settings);
        assertNotSame(null, settings);
    }

    */
/**
     * Test of save method, of class Settings.
     *//*

    @Test
    public void testSave() throws Exception {

        Settings settings = new Settings(ROWS, COLUMNS, MINES);
        settings.save();
        File f = new File(SETTING_FILE);
        assertNotSame(null, f);

    }

    */
/**
     * Test of load method, of class Settings.
     *//*

    @Test
    public void testLoad() throws Exception {
        File f = new File(SETTING_FILE);
        if (f.exists()) {
            Settings settings = Settings.load();
            assertNotSame(settings, null);
        } else {
            Settings settings = Settings.load();
            assertNotSame(settings, Settings.BEGINNER);
        }
    }

    */
/**
     * Test of toString method, of class Settings.
     *//*

    @Test
    public void testToString() {
        Settings settings1 = new Settings(9, 9, 10);
        assertEquals(Settings.BEGINNER.toString(), "beginner");

        Settings settings2 = new Settings(16, 16, 40);
        assertEquals(settings2.toString(), "intermediate");

        Settings settings3 = new Settings(16, 30, 99);
        assertEquals(settings3.toString(), "expert");
    }

    @Test
    public void saveAndLoad() throws IOException, ClassNotFoundException {
        Settings saved = new Settings(ROWS, COLUMNS, MINES);
        saved.save();

        Settings loaded = Settings.load();
        assertEquals(saved, loaded);
    }

}
*/
