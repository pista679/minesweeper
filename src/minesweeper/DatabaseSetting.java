/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

/**
 *
 * @author pista
 */
public class DatabaseSetting {

    public static final String DRIVER_CLASS = "org.apache.derby.jdbc.ClientDriver";
    public static final String URL = "jdbc:derby://localhost/minesweeper";
    public static final String USER = "minesweeper";
    public static final String PASSWORD = "minesweeper";

    public static final String QUERY_CREATE_BEST_TIMES = "CREATE TABLE player_time (name VARCHAR(128) NOT NULL, best_time INT NOT NULL, level VARCHAR(20))";
    public static final String QUERY_ADD_BEST_TIME = "INSERT INTO player_time (name, best_time, level) VALUES (?, ?, ?)";
    public static final String QUERY_SELECT_BEST_TIMES = "SELECT name, best_time, level FROM player_time";
    public static final String QUERY_SELECT_BEST_TIME = "SELECT best_time FROM player_time ORDER BY 1 ASC";

    private DatabaseSetting() {
    }
}
