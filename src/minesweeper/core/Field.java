package minesweeper.core;

import java.util.Random;

/**
 * Field represents playing field and game logic.
 */
public class Field {

    /**
     * Playing field tiles.
     */
    private final Tile[][] tiles;

    /**
     * Field row count. Rows are indexed from 0 to (rowCount - 1).
     */
    private final int rowCount;

    /**
     * Column count. Columns are indexed from 0 to (columnCount - 1).
     */
    private final int columnCount;

    /**
     * Mine count.
     */
    private final int mineCount;

    /**
     * Game state.
     */
    private GameState state = GameState.PLAYING;
    
    

    /**
     * Constructor.
     *
     * @param rowCount row count
     * @param columnCount column count
     * @param mineCount mine count
     */
    public Field(int rowCount, int columnCount, int mineCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.mineCount = mineCount;
        tiles = new Tile[rowCount][columnCount];
        generate();
    }

    /**
     * Opens tile at specified indeces.
     *
     * @param row row number
     * @param column column number
     */
    public void openTile(int row, int column) {
        final Tile tile = tiles[row][column];
        if (tile.getState() == Tile.State.CLOSED) {
            tile.setState(Tile.State.OPEN);
            if (tile instanceof Mine) {
                setState(GameState.FAILED);
                return;
            }
            if (tile instanceof Clue) {
                if (((Clue) tile).getValue() == 0) {
                    openAdjacentTiles(row, column);
                }
            }

            if (isSolved()) {
                setState(GameState.SOLVED);
            }
        }
    }
    
    

    /**
     * Marks tile at specified indeces.
     *
     * @param row row number
     * @param column column number
     */
    public void markTile(int row, int column) {
        final Tile tile = tiles[row][column];
        if (tile.getState() == Tile.State.CLOSED) {
            tile.setState(Tile.State.MARKED);
        } else if (tile.getState() == Tile.State.MARKED) {
            tile.setState(Tile.State.QUESTION);
        } else if (tile.getState() == Tile.State.QUESTION) {
            tile.setState(Tile.State.CLOSED);
        }
    }

    /**
     * Generates playing field.
     */
    private void generate() {
        generateMines();
        fillWithClues();
    }

    /**
     * Generates mines in field.
     */
    private void generateMines() {
        Random n = new Random();
        int counter = 0;
        while (getMineCount() != counter) {
            int row = n.nextInt(getRowCount());
            int column = n.nextInt(getColumnCount());
            if (getTile(row, column) == null) {
                tiles[row][column] = new Mine();
                counter++;
            }
        }

    }

    /**
     * Generates clues with number of adjacent mines.
     */
    private void fillWithClues() {
        for (int row = 0; row < getRowCount(); row++) {
            for (int column = 0; column < getColumnCount(); column++) {
                if (getTile(row, column) == null) {
                    tiles[row][column] = new Clue(countAdjacentMines(row, column));
                }
            }
        }

    }

    /**
     * Returns true if game is solved, false otherwise.
     *
     * @return true if game is solved, false otherwise
     */
    private boolean isSolved() {
        if ((rowCount * columnCount - getNumberOf(Tile.State.OPEN) == getMineCount())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns number of adjacent mines for a tile at specified position in the
     * field.
     *
     * @param row row number.
     * @param column column number.
     * @return number of adjacent mines.
     */
    private int countAdjacentMines(int row, int column) {
        int count = 0;

        for (int rowOffset = -1; rowOffset <= 1; rowOffset++) {
            int actRow = row + rowOffset;
            if (actRow >= 0 && actRow < getRowCount()) {
                for (int columnOffset = -1; columnOffset <= 1; columnOffset++) {
                    int actColumn = column + columnOffset;
                    if (actColumn >= 0 && actColumn < getColumnCount()) {
                        if (getTile(actRow, actColumn) instanceof Mine) {
                            count++;
                        }
                    }
                }
            }
        }

        return count;
    }

    /**
     * @return the rowCount
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * @return the columnCount
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * @return the mineCount
     */
    public int getMineCount() {
        return mineCount;
    }

    /**
     * @return the state
     */
    public GameState getState() {
        if (state == null){
            return GameState.PLAYING;
        }
        return state;
    }

    /**
     * Returns tile at specified position in the field.
     *
     * @param row row number.
     * @param column column number.
     * @return tile.
     */
    public Tile getTile(int row, int column) {
        return tiles[row][column];
    }

    /**
     * Returns number of mines with specified state.
     *
     * @param state state of tile
     * @return number of mines with specified state.
     */
    private int getNumberOf(Tile.State state) {
        int counter = 0;

        for (int row = 0; row < getRowCount(); row++) {
            for (int column = 0; column < getColumnCount(); column++) {
                Tile tile = tiles[row][column];
                if (tile.getState() == state) {
                    counter++;
                }
            }
        }
        return counter;
    }

    /**
     * Opens tiles around a tile at specified position in the field.
     *
     * @param row row number.
     * @param column column number.
     */
    private void openAdjacentTiles(int row, int column) {
        for (int rowOffset = -1; rowOffset <= 1; rowOffset++) {
            int actRow = row + rowOffset;
            if (actRow >= 0 && actRow < getRowCount()) {
                for (int columnOffset = -1; columnOffset <= 1; columnOffset++) {
                    int actColumn = column + columnOffset;
                    if (actColumn >= 0 && actColumn < getColumnCount()) {
                        openTile(actRow, actColumn);
                    }
                }
            }
        }
    }

    /**
     * Returns number of mines descreased by number of marked tiles.
     *
     * @return number remaining mines.
     */
    public int getRemainingMineCount() {
        return mineCount - getNumberOf(Tile.State.MARKED);
    }

    /**
     * @param state the state to set
     */
    public void setState(GameState state) {
        this.state = state;
    }
}
