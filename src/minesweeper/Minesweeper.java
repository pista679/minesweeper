package minesweeper;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import minesweeper.consoleui.ConsoleUI;
import minesweeper.core.Field;
import minesweeper.swingui.SwingUI;

/**
 * Main application class.
 */
public class Minesweeper {

    /**
     * User interface.
     */
    private UserInterface userInterface;

    /**
     * Time of start
     */
    private long startMillis;

    /**
     * Instance of Minesweeper
     */
    private static Minesweeper instance;

    /**
     * Best times
     */
    private BestTimes bestTimes;

    /**
     * Setting
     */
    private Settings setting;

    /**
     * Defined user interface
     */
    private static String DEFAULT_UI;

    /**
     * Constructor.
     */
    public Minesweeper() throws ClassNotFoundException, IOException {
        instance = this;
        setting = Settings.load();
        userInterface = create(DEFAULT_UI);
        newGame();
        startMillis = System.currentTimeMillis();
        bestTimes = new BestTimes();
    }

    /**
     * Creates new game
     */
    public void newGame() {
        Field field = new Field(setting.getRowCount(), setting.getColumnCount(), setting.getMineCount());
        startMillis = System.currentTimeMillis();
        userInterface.newGameStarted(field);
    }

    /**
     * @return the time of playing in seconds
     */
    public int getPlayingSeconds() {
        return (int) ((System.currentTimeMillis() - startMillis) / 1000);
    }

    /**
     * Main method.
     *
     * @param args arguments
     * @throws java.lang.ClassNotFoundException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws ClassNotFoundException, IOException {
        if (args.length > 0 && args[0].equals("console")){
            DEFAULT_UI = "console";
        } else{
            DEFAULT_UI = "swing";
        }
        new Minesweeper();
    }

    /**
     * @return the bestTimes
     */
    public BestTimes getBestTimes() {
        return bestTimes;
    }

    /**
     * @return the instance
     */
    public static Minesweeper getInstance() {
        if (instance == null) {
            try {
                instance = new Minesweeper();
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Minesweeper.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Minesweeper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return instance;
    }

    /**
     * @return the setting
     */
    public Settings getSetting() {
        return setting;
    }

    /**
     * @param setting the setting to set
     */
    public void setSetting(Settings setting) {
        this.setting = setting;
        try {
            setting.save();
        } catch (IOException ex) {
            Logger.getLogger(Minesweeper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Create new interface
     * @param name the name of defined interface
     * @return user interface
     */
    private UserInterface create(String name) {
        if (name.equals("swing")) {
            return new SwingUI();
        } else if (name.equals("console")) {
            return new ConsoleUI();
        } else {
            throw new RuntimeException("Wrong input");
        }
    }

}
