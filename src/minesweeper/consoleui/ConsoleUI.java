package minesweeper.consoleui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import minesweeper.Minesweeper;
import minesweeper.UserInterface;
import minesweeper.core.Clue;
import minesweeper.core.Field;
import minesweeper.core.GameState;
import minesweeper.core.Mine;
import minesweeper.core.Tile;

/**
 * Console user interface.
 */
public class ConsoleUI implements UserInterface {

    /**
     * Playing field.
     */
    private Field field;

    /**
     * Input reader.
     */
    private BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    /**
     * Reads line of text from the reader.
     *
     * @return line as a string
     */
    private String readLine() {
        try {
            return input.readLine();
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Starts the game.
     *
     * @param field field of mines and clues
     */
    @Override
    public void newGameStarted(Field field) {
        this.field = field;
        do {
            update();
            processInput();
            //handleInput();
            if (field.getState() == GameState.SOLVED) {
                System.out.println("Parada");
                System.exit(0);
            }
            if (field.getState() == GameState.FAILED) {
                System.out.println("Ne bars parada");
                System.exit(0);
            }
        } while (true);
    }

    /**
     * Updates user interface - prints the field.
     */
    @Override
    public void update() {
        System.out.println("To mark: " + field.getRemainingMineCount() + " Time: " + Minesweeper.getInstance().getPlayingSeconds() + "s");
        System.out.println("  0 1 2 3 4 5 6 7 8");

        for (int row = 0; row < field.getRowCount(); row++) {
            printLetter(row);
            for (int column = 0; column < field.getColumnCount(); column++) {
                printTile(row, column);
            }
            System.out.print("\n");
        }

        System.out.println("Please enter your selection:");
    }

    /**
     * Prints single tile at specified position
     * @param row 
     * @param column 
     */
    private void printTile(int row, int column) {
        Tile tile = field.getTile(row, column);
        switch (tile.getState()) {
            case CLOSED:
                System.out.print(" -");
                break;
            case QUESTION:
                System.out.print(" ?");
                break;
            case MARKED:
                System.out.print(" M");
                break;
            case OPEN:
                if (tile instanceof Mine) {
                    System.out.print(" X");
                }
                if (tile instanceof Clue) {
                    System.out.print(" " + ((Clue) tile).getValue());
                }
                break;
        }
    }

    /**
     * Prints letter for each row
     * @param row 
     */
    private void printLetter(int row) {
        switch (row) {
            case 0:
                System.out.print("A");
                break;
            case 1:
                System.out.print("B");
                break;
            case 2:
                System.out.print("C");
                break;
            case 3:
                System.out.print("D");
                break;
            case 4:
                System.out.print("E");
                break;
            case 5:
                System.out.print("F");
                break;
            case 6:
                System.out.print("G");
                break;
            case 7:
                System.out.print("H");
                break;
            case 8:
                System.out.print("I");
                break;

        }
    }

    /**
     * Processes user input. Reads line from console and does the action on a
     * playing field according to input string.
     */
    private void processInput() {

//        String vstup2=readLine();
//        String vstup = vstup2.toUpperCase();
        try {
            handleInput();
        } catch (WrongFormatException e) {
            System.out.print(e);
            processInput();
        }

//        //end of game
//        Pattern pat = Pattern.compile("[X,x]");
//        Matcher match1=pat.matcher(vstup);
//        
//        //mark         
//        Pattern pat2 = Pattern.compile("[M,m]([A-I])([0-8])");
//        Matcher match2=pat2.matcher(vstup);
//         
//        //open
//        Pattern pat3 = Pattern.compile("[O,o]([A-I])([0-8])");
//        Matcher match3=pat3.matcher(vstup);
//         
//        if(match1.find())
//        {
//            System.exit(0);
//        }
//        else if (match2.find()){
//            char data[]=vstup.toCharArray();
//            int x=data[1]-65;
//            int y =data[2]-48;
//            
//            field.markTile(x, y);
//        }
//        else if (match3.find()){
//            char data[]=vstup.toCharArray();
//            int x=data[1]-65;
//            int y =data[2]-48;
//             
//            field.openTile(x, y);
//        }else {
//            processInput();
//        }
//        
    }

    /**
     * Handles input
     * @throws WrongFormatException 
     */
    private void handleInput() throws WrongFormatException {

        String vstup2 = readLine();

        if (vstup2 == null) {
            throw new WrongFormatException("\nNo input\n");
        }

        String vstup = vstup2.toUpperCase();

        //end of game
        Pattern pat = Pattern.compile("X");
        Matcher match1 = pat.matcher(vstup);

        //mark         
        Pattern pat2 = Pattern.compile("M([A-I])([0-8])");
        Matcher match2 = pat2.matcher(vstup);

        //open
        Pattern pat3 = Pattern.compile("O([A-I])([0-8])");
        Matcher match3 = pat3.matcher(vstup);

        if (match1.find()) {
            System.exit(0);
        } else if (match2.find()) {
            char data[] = vstup.toCharArray();
            int x = data[1] - 65;
            int y = data[2] - 48;

            field.markTile(x, y);
        } else if (match3.find()) {
            char data[] = vstup.toCharArray();
            int x = data[1] - 65;
            int y = data[2] - 48;

            field.openTile(x, y);
        } else {
            throw new WrongFormatException("\nWrong input\n");
        }

    }
}
