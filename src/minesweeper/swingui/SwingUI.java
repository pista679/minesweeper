package minesweeper.swingui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import minesweeper.BestTimes;
import minesweeper.Minesweeper;
import minesweeper.Settings;
import minesweeper.UserInterface;
import minesweeper.core.Field;
import minesweeper.core.GameState;
import minesweeper.core.Mine;
import minesweeper.core.Tile;

public class SwingUI extends javax.swing.JFrame implements UserInterface, MouseListener {

    /**
     * Field
     */
    private Field field;
    /**
     * Timer
     */
    private javax.swing.Timer timer;
    /**
     * Sets the difference between normal mode and beating best time mode
     */
    private boolean beatingBestTime;

    private boolean hintEnabled = false;

    private ArrayList<Tile> hintedTiles;

    public SwingUI() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        beatingBestTime = false;

        hintedTiles = new ArrayList();

        initComponents();
        Minesweeper.getInstance();

        setIconImage(new javax.swing.ImageIcon(getClass().getResource("/img/logo.gif")).getImage());
        setVisible(true);

//        if (Minesweeper.getInstance() == null){
//            return;
//        }
        if (Minesweeper.getInstance().getSetting().equals(Settings.BEGINNER)) {
            beginnerMenuItem.setSelected(true);
        } else if (Minesweeper.getInstance().getSetting().equals(Settings.INTERMEDIATE)) {
            intermediateMenuItem.setSelected(true);
        } else if (Minesweeper.getInstance().getSetting().equals(Settings.EXPERT)) {
            expertMenuItem.setSelected(true);
        }

        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (field.getState() == GameState.PLAYING) {
                    setTimeLabelText();
                }
            }
        };

        timer = new Timer(1, listener);
        timer.start();
    }

    @Override
    public void newGameStarted(Field field) {
        this.field = field;
        contentPanel.removeAll();
        contentPanel.setLayout(new GridLayout(field.getRowCount(), field.getColumnCount()));

        for (int i = 0; i < field.getRowCount(); i++) {
            for (int j = 0; j < field.getColumnCount(); j++) {
                contentPanel.add(new TileComponent(field.getTile(i, j), i, j)).addMouseListener(this);
            }
        }
        update();
        pack();

    }

    /**
     * Switch game mode
     */
    public void beatBestTime() {
        if (beatingBestTime) {
            beatingBestTime = false;
        } else {
            beatingBestTime = true;
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

        if (field.getState() == GameState.PLAYING && SwingUtilities.isMiddleMouseButton(e)) {
            TileComponent tc = (TileComponent) (e.getComponent());
            openAdjacentTiles(tc.getRow(), tc.getColumn());
        } else if (field.getState() == GameState.PLAYING && SwingUtilities.isLeftMouseButton(e)) {
            TileComponent tc = (TileComponent) (e.getComponent());
            if (hintEnabled && (field.getTile(tc.getRow(), tc.getColumn()) instanceof Mine)) {

                if (hintedTiles.contains(field.getTile(tc.getRow(), tc.getColumn()))) {
                    field.openTile(tc.getRow(), tc.getColumn());
                } else {
                    JOptionPane.showMessageDialog(this, "Watch out, there is mine", "Watch out", JOptionPane.WARNING_MESSAGE);
                    hintedTiles.add(field.getTile(tc.getRow(), tc.getColumn()));
                }
            } else {
                field.openTile(tc.getRow(), tc.getColumn());
            }
            //update();
//        } else if ( field.getState() == GameState.FAILED){
//            JOptionPane.showMessageDialog(this, "Nesi pan", "LOSE :-(", JOptionPane.ERROR_MESSAGE);
        } else if (SwingUtilities.isRightMouseButton(e)) {
            TileComponent tc = (TileComponent) (e.getComponent());
            field.markTile(tc.getRow(), tc.getColumn());
            //update();
//        } else if(field.getState() == GameState.SOLVED){
//            BestTimes bs = Minesweeper.getInstance().getBestTimes();
//            bs.addPlayerTime(System.getProperty("user.name"), Minesweeper.getInstance().getPlayingSeconds());
//            JOptionPane.showMessageDialog(this, "Si pan", "WIN :-)", JOptionPane.WARNING_MESSAGE);
        }
        update();
    }

    private void hintMode() {
        if (hintEnabled) {
            hintEnabled = false;
        } else {
            hintEnabled = true;
        }
    }

    /**
     * Opens adjacent tiles of tile at specified position
     *
     * @param row
     * @param column
     */
    public void openAdjacentTiles(int row, int column) {
        for (int rowOffset = -1; rowOffset <= 1; rowOffset++) {
            int actRow = row + rowOffset;
            if (actRow >= 0 && actRow < field.getRowCount()) {
                for (int columnOffset = -1; columnOffset <= 1; columnOffset++) {
                    int actColumn = column + columnOffset;
                    if (actColumn >= 0 && actColumn < field.getColumnCount()) {
                        if ((actColumn != column || actRow != row) && field.getTile(actRow, actColumn).getState() != Tile.State.MARKED) {
                            field.openTile(actRow, actColumn);
                        }
                    }
                }
            }
        }
    }

    /**
     * Set number of remainig mines
     */
    private void setMinesLeftLabelText() {
        StringBuilder sb = new StringBuilder();
        new Formatter(sb).format("%03d", field.getRemainingMineCount());

        minesLeftLabel.setText(sb.toString());
    }

    /**
     * Set time
     */
    private void setTimeLabelText() {
        StringBuilder sb = new StringBuilder();
        if (beatingBestTime) {
            new Formatter(sb).format("%03d", Minesweeper.getInstance().getBestTimes().getBestTime() - Minesweeper.getInstance().getPlayingSeconds());
            if ((Minesweeper.getInstance().getBestTimes().getBestTime() - Minesweeper.getInstance().getPlayingSeconds()) < 1) {
                field.setState(GameState.FAILED);
                if (field.getState() == GameState.SOLVED) {
                    JOptionPane.showMessageDialog(this, "NEW RECORD", "WIN :-)", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                update();
            }
        } else {
            new Formatter(sb).format("%03d", Minesweeper.getInstance().getPlayingSeconds());
        }

        timeLabel.setText(sb.toString());
    }

    @Override
    public void update() {
        int counter = contentPanel.getComponentCount();

        for (int i = 0; i < counter; i++) {
            TileComponent temp = (TileComponent) contentPanel.getComponent(i);
            temp.updateStyle();
        }
        setMinesLeftLabelText();

        if (field.getState() == GameState.FAILED) {
            JOptionPane.showMessageDialog(this, "You lost", "LOSE :-(", JOptionPane.ERROR_MESSAGE);
        } else if (field.getState() == GameState.SOLVED) {
            BestTimes bs = Minesweeper.getInstance().getBestTimes();
            try {
                bs.addPlayerTime(System.getProperty("user.name"), Minesweeper.getInstance().getPlayingSeconds(), Minesweeper.getInstance().getSetting().toString());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(SwingUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(SwingUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(this, "Congratulations, you win", "WIN :-)", JOptionPane.WARNING_MESSAGE);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        topPanel = new javax.swing.JPanel();
        infoPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        minesLeftLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        timeLabel = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        newButton = new javax.swing.JButton();
        contentPanel = new javax.swing.JPanel();
        menuBar = new javax.swing.JMenuBar();
        gameMenu = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        beginnerMenuItem = new javax.swing.JRadioButtonMenuItem();
        intermediateMenuItem = new javax.swing.JRadioButtonMenuItem();
        expertMenuItem = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        bestTimesMenuItem = new javax.swing.JMenuItem();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        exitMenuItem = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Minesweeper");
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(new java.awt.BorderLayout());

        topPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 2, 5), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED)));
        topPanel.setLayout(new java.awt.BorderLayout());

        infoPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(4, 4, 4, 4));
        infoPanel.setLayout(new java.awt.BorderLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel2.setLayout(new java.awt.BorderLayout());

        minesLeftLabel.setBackground(java.awt.Color.black);
        minesLeftLabel.setFont(new java.awt.Font("DialogInput", 1, 24)); // NOI18N
        minesLeftLabel.setForeground(java.awt.Color.red);
        minesLeftLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        minesLeftLabel.setText("888");
        minesLeftLabel.setMaximumSize(new java.awt.Dimension(50, 30));
        minesLeftLabel.setMinimumSize(new java.awt.Dimension(50, 30));
        minesLeftLabel.setOpaque(true);
        minesLeftLabel.setPreferredSize(new java.awt.Dimension(50, 30));
        jPanel2.add(minesLeftLabel, java.awt.BorderLayout.CENTER);

        infoPanel.add(jPanel2, java.awt.BorderLayout.WEST);

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jPanel3.setLayout(new java.awt.BorderLayout());

        timeLabel.setBackground(java.awt.Color.black);
        timeLabel.setFont(new java.awt.Font("DialogInput", 1, 24)); // NOI18N
        timeLabel.setForeground(java.awt.Color.red);
        timeLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        timeLabel.setText("888");
        timeLabel.setMaximumSize(new java.awt.Dimension(50, 30));
        timeLabel.setMinimumSize(new java.awt.Dimension(50, 30));
        timeLabel.setOpaque(true);
        timeLabel.setPreferredSize(new java.awt.Dimension(50, 30));
        jPanel3.add(timeLabel, java.awt.BorderLayout.CENTER);

        infoPanel.add(jPanel3, java.awt.BorderLayout.EAST);

        newButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/smile.gif"))); // NOI18N
        newButton.setFocusPainted(false);
        newButton.setFocusable(false);
        newButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        newButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newButtonActionPerformed(evt);
            }
        });
        jPanel4.add(newButton);

        infoPanel.add(jPanel4, java.awt.BorderLayout.CENTER);

        topPanel.add(infoPanel, java.awt.BorderLayout.CENTER);

        jPanel1.add(topPanel, java.awt.BorderLayout.NORTH);

        contentPanel.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEmptyBorder(3, 5, 5, 5), javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED)));
        jPanel1.add(contentPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        gameMenu.setMnemonic('g');
        gameMenu.setText("Game");

        newMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        newMenuItem.setMnemonic('n');
        newMenuItem.setText("New");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(newMenuItem);
        gameMenu.add(jSeparator1);

        buttonGroup.add(beginnerMenuItem);
        beginnerMenuItem.setMnemonic('b');
        beginnerMenuItem.setText("Beginner");
        beginnerMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beginnerMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(beginnerMenuItem);

        buttonGroup.add(intermediateMenuItem);
        intermediateMenuItem.setMnemonic('i');
        intermediateMenuItem.setText("Intermediate");
        intermediateMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                intermediateMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(intermediateMenuItem);

        buttonGroup.add(expertMenuItem);
        expertMenuItem.setMnemonic('e');
        expertMenuItem.setText("Expert");
        expertMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expertMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(expertMenuItem);

        jRadioButtonMenuItem1.setText("Custom");
        jRadioButtonMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem1ActionPerformed(evt);
            }
        });
        gameMenu.add(jRadioButtonMenuItem1);

        bestTimesMenuItem.setText("Best times...");
        bestTimesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bestTimesMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(bestTimesMenuItem);

        jRadioButtonMenuItem2.setText("Beat best time");
        jRadioButtonMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem2ActionPerformed(evt);
            }
        });
        gameMenu.add(jRadioButtonMenuItem2);
        gameMenu.add(jSeparator3);
        gameMenu.add(jSeparator2);

        exitMenuItem.setMnemonic('e');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        gameMenu.add(exitMenuItem);
        gameMenu.add(jSeparator4);

        jCheckBoxMenuItem1.setText("hintMode");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        gameMenu.add(jCheckBoxMenuItem1);

        menuBar.add(gameMenu);

        setJMenuBar(menuBar);
    }// </editor-fold>//GEN-END:initComponents

    private void newButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newButtonActionPerformed
        newMenuItemActionPerformed(null);
    }//GEN-LAST:event_newButtonActionPerformed

    private void expertMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expertMenuItemActionPerformed
        Minesweeper.getInstance().setSetting(Settings.EXPERT);
        Minesweeper.getInstance().newGame();
    }//GEN-LAST:event_expertMenuItemActionPerformed

    private void intermediateMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_intermediateMenuItemActionPerformed
        Minesweeper.getInstance().setSetting(Settings.INTERMEDIATE);
        Minesweeper.getInstance().newGame();
    }//GEN-LAST:event_intermediateMenuItemActionPerformed

    private void beginnerMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_beginnerMenuItemActionPerformed
        Minesweeper.getInstance().setSetting(Settings.BEGINNER);
        Minesweeper.getInstance().newGame();
    }//GEN-LAST:event_beginnerMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemActionPerformed
        Minesweeper.getInstance().newGame();
    }//GEN-LAST:event_newMenuItemActionPerformed

    private void bestTimesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bestTimesMenuItemActionPerformed
        new BestTimesDialog(this, true).setVisible(true);
    }//GEN-LAST:event_bestTimesMenuItemActionPerformed

    private void jRadioButtonMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem1ActionPerformed
        new CustomSettingDialog(this, true).setVisible(true);
    }//GEN-LAST:event_jRadioButtonMenuItem1ActionPerformed

    private void jRadioButtonMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem2ActionPerformed
        beatBestTime();
    }//GEN-LAST:event_jRadioButtonMenuItem2ActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        hintMode();
    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButtonMenuItem beginnerMenuItem;
    private javax.swing.JMenuItem bestTimesMenuItem;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JPanel contentPanel;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JRadioButtonMenuItem expertMenuItem;
    private javax.swing.JMenu gameMenu;
    private javax.swing.JPanel infoPanel;
    private javax.swing.JRadioButtonMenuItem intermediateMenuItem;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JLabel minesLeftLabel;
    private javax.swing.JButton newButton;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JLabel timeLabel;
    private javax.swing.JPanel topPanel;
    // End of variables declaration//GEN-END:variables
}
