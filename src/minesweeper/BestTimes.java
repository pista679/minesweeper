/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Player times.
 */
public class BestTimes implements Iterable<BestTimes.PlayerTime> {

    /**
     * List of best player times.
     */
    private List<PlayerTime> playerTimes = new ArrayList<PlayerTime>();
    private PlayerTime playerTime;

    /**
     * Returns an iterator over a set of best times.
     *
     * @return an iterator
     */
    @Override
    public Iterator<PlayerTime> iterator() {
        return playerTimes.iterator();
    }

    /**
     * Adds player time to best times.
     *
     * @param name name ot the player
     * @param time player time in seconds
     * @param level player level
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public void addPlayerTime(String name, int time, String level) throws ClassNotFoundException, SQLException {
        playerTime = new PlayerTime(name, time, level);
        //playerTimes.add(pl);
        Collections.sort(playerTimes);
        insertToDB(playerTime);

    }

    /**
     * Adds player time to best times.
     *
     * @param name name ot the player
     * @param time player time in seconds
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public void addPlayerTime(String name, int time) throws ClassNotFoundException, SQLException {
        playerTime = new PlayerTime(name, time);
        //playerTimes.add(pl);
        Collections.sort(playerTimes);
        insertToDB(playerTime);

    }

    /**
     * Returns a string representation of the object.
     *
     * @return a string representation of the object
     */
    @Override
    public String toString() {
        try {
            selectFromDB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BestTimes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BestTimes.class.getName()).log(Level.SEVERE, null, ex);
        }
        Formatter f = new Formatter();
        f.format("    name - time - level\n------------------------------\n");
        int i = 1;
        for (PlayerTime p : playerTimes) {
            f.format("%d. ", i);
            f.format("%s   -", p.getName());
            f.format(" %ss  -", p.getTime());
            f.format(" %s \n", p.getLevel());
            i++;
        }

        return f.toString();
    }

    /**
     * Clear player times
     */
    public void reset() {
        playerTimes.clear();
    }

    /**
     * Select player times from database and add to array playerTimes in ascending order
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    private void selectFromDB() throws ClassNotFoundException, SQLException {
        reset();

        Class.forName(DatabaseSetting.DRIVER_CLASS);
        Connection connection = DriverManager.getConnection(DatabaseSetting.URL,
                DatabaseSetting.USER, DatabaseSetting.PASSWORD);

        Statement stm = connection.createStatement();
        ResultSet rs = stm.executeQuery(DatabaseSetting.QUERY_SELECT_BEST_TIMES);

        while (rs.next()) {
            PlayerTime pt = new PlayerTime(rs.getString(1), rs.getInt(2), rs.getString(3));
            playerTimes.add(pt);
        }
        stm.close();
        Collections.sort(playerTimes);
    }

    /**
     * Insert player time to database
     * @param playerTime
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    private void insertToDB(PlayerTime playerTime) throws ClassNotFoundException, SQLException {
        Class.forName(DatabaseSetting.DRIVER_CLASS);
        Connection connection = DriverManager.getConnection(DatabaseSetting.URL,
                DatabaseSetting.USER, DatabaseSetting.PASSWORD);

        Statement stm = connection.createStatement();
        try {
            stm.executeUpdate(DatabaseSetting.QUERY_CREATE_BEST_TIMES);
        } catch (Exception e) {
            //do not propagate exception, table may already exist
        }
        stm.close();

        PreparedStatement pstm = connection.prepareStatement(DatabaseSetting.QUERY_ADD_BEST_TIME);
        pstm.setString(1, playerTime.getName());
        pstm.setInt(2, playerTime.getTime());
        pstm.setString(3, playerTime.getLevel());
        pstm.execute();
        pstm.close();

    }

    /**
     * Delete all rows from database
     * @throws ClassNotFoundException
     * @throws SQLException 
     */
    public void deleteDB() throws ClassNotFoundException, SQLException {
        Class.forName(DatabaseSetting.DRIVER_CLASS);
        Connection connection = DriverManager.getConnection(DatabaseSetting.URL,
                DatabaseSetting.USER, DatabaseSetting.PASSWORD);

        Statement stm = connection.createStatement();
        try {
            stm.executeUpdate("TRUNCATE table player_time");
        } catch (Exception e) {
            //do not propagate exception, table may already exist
        }
        stm.close();

        System.out.print("deleted!!");

    }

    /**
     * 
     * @return best time 
     */
    public int getBestTime() {
        try {
            selectFromDB();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BestTimes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(BestTimes.class.getName()).log(Level.SEVERE, null, ex);
        }

//        PlayerTime time = null;
//        for (PlayerTime p : playerTimes){
//            time = p;
//        }
//        
//        return time.time;
        return playerTimes.get(0).getTime();
    }

    /**
     * Player time.
     */
    public static class PlayerTime implements Comparable<PlayerTime> {

        /**
         * Player name.
         */
        private final String name;

        /**
         * Playing time in seconds.
         */
        private final int time;

        /**
         * Player current settings.
         */
        private final String level;

        /**
         * Constructor.
         *
         * @param name player name
         * @param time playing game time in seconds
         */
        public PlayerTime(String name, int time) {
            this.name = name;
            this.time = time;
            this.level = null;
        }

        /**
         * 
         * @param name player name
         * @param time playing game time in seconds
         * @param level current level
         */
        public PlayerTime(String name, int time, String level) {
            this.name = name;
            this.time = time;
            this.level = level;
        }

        @Override
        public int compareTo(PlayerTime o) {
            return getTime() - o.getTime();
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the time
         */
        public int getTime() {
            return time;
        }

        /**
         * @return the level
         */
        public String getLevel() {
            return level;
        }

    }
}
