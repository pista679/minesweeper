/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *
 * @author pista
 */
public class Settings implements Serializable {

    /**
     * Number of rows
     */
    private final int rowCount;
    /**
     * Number of columns
     */
    private final int columnCount;
    /**
     * Number of mines
     */
    private final int mineCount;

    /**
     * Beginner settings
     */
    public static final Settings BEGINNER = new Settings(9, 9, 10);

    /**
     * Intermediate settings
     */
    public static final Settings INTERMEDIATE = new Settings(16, 16, 40);

    /**
     * Expert settings
     */
    public static final Settings EXPERT = new Settings(16, 30, 99);

    /**
     * Setting file
     */
    private static final String SETTING_FILE = System.getProperty("user.home") + System.getProperty("file.separator") + "minesweeper.settings";

    /**
     * Constructor
     * @param rowCount number of rows
     * @param columnCount number of columns
     * @param mineCount number of mines
     */
    public Settings(int rowCount, int columnCount, int mineCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.mineCount = mineCount;
    }

    /**
     * @return the rowCount
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * @return the columnCount
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * @return the mineCount
     */
    public int getMineCount() {
        return mineCount;
    }

    @Override
    public int hashCode() {
        return rowCount * columnCount * mineCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Settings other = (Settings) obj;
        if (this.rowCount != other.rowCount) {
            return false;
        }
        if (this.columnCount != other.columnCount) {
            return false;
        }
        if (this.mineCount != other.mineCount) {
            return false;
        }
        return true;
    }

    /**
     * Save the current settings
     * @throws java.io.IOException
     */
    public void save() throws IOException {
        FileOutputStream file = null;
        try {
            file = new FileOutputStream(SETTING_FILE);
        } catch (FileNotFoundException fileNotFoundException) {
        }

        ObjectOutputStream stream = null;
        try {
            stream = new ObjectOutputStream(file);
        } catch (IOException iOException) {
        }

        stream.writeObject(this);
        stream.close();
    }

    /**
     * Load new settings
     *
     * @return the loaded settings
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static Settings load() throws ClassNotFoundException, IOException {

        File f = new File(SETTING_FILE);

        if (!f.exists()) {
            return BEGINNER;
        }

        FileInputStream file = null;
        try {
            file = new FileInputStream(SETTING_FILE);
        } catch (FileNotFoundException fileNotFoundException) {
        }

        ObjectInputStream stream = null;
        try {
            stream = new ObjectInputStream(file);
        } catch (IOException iOException) {
        }

        return (Settings) stream.readObject();
    }

    @Override
    public String toString() {
        if (this.equals(BEGINNER)) {
            return "beginner";
        }
        if (this.equals(INTERMEDIATE)) {
            return "intermediate";
        }
        if (this.equals(EXPERT)) {
            return "expert";
        }
        return "custom (" + this.getRowCount() + "x" + this.getColumnCount() + "x" + this.getMineCount() + ")" ;
    }
}
